package com.se499.bioscanner;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.net.URI;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import static android.support.v4.app.ActivityCompat.startActivity;
public class ResultsAdapter extends ArrayAdapter<ResListEntry> {
    private final Context context;
    private final ResListEntry[] values;
    private LayoutInflater inflater;
    public ResultsAdapter(Context context, ResListEntry[] values) {
        super(context, R.layout.result_list_entry, values);
        this.context = context;
        this.values = values;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            convertView=inflater.inflate(R.layout.result_list_entry,null);
            convertView.setClickable(true);
            holder=new ViewHolder();
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.icon);
            holder.firstLine = (TextView) convertView.findViewById(R.id.firstLine);
            holder.secondLine = (TextView) convertView.findViewById(R.id.secondLine);
            convertView.setTag(holder);
        }
        else {
            holder=(ViewHolder)convertView.getTag();
        }
         View.OnClickListener ocl = new View.OnClickListener()
        {
              public void onClick(View v)
              {
                  Intent intent=new Intent(context,DetailResultActivity.class);
                  intent.putExtra(ResultsScreen.RESULT_POSITION,position);
                  context.startActivity(intent);
                  //Toast.makeText(v.getContext(), "Saved", Toast.LENGTH_LONG).show();
              }
        };
        //ImageView imageView = (ImageView) convertView.findViewById(R.id.icon);
        //holder.thumbnail=imageView;
        holder.firstLine.setText(values[position].f.getName());
        long timeAgo=(new Date().getTime() - values[position].f.lastModified())-(19*3600000);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateFormatted = formatter.format(timeAgo);
        if(values[position].isBad==1) {
            holder.secondLine.setBackgroundColor(Color.RED);
            holder.secondLine.setText("DETECTED"+" - "+dateFormatted+" ago");
        }else{
            holder.secondLine.setText("CLEAN"+" - "+dateFormatted+" ago");
            holder.secondLine.setBackgroundColor(Color.GREEN);
        }


        //do image resizing on background thread
        if(holder.position!=position) {
            new ThumbnailTask(position, holder)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, values[position].f.getAbsolutePath());
        }
        holder.position=position;

        //TextView firstLineTextView = (TextView) convertView.findViewById(R.id.firstLine);
        //TextView secondLineTextView = (TextView) convertView.findViewById(R.id.secondLine);

        convertView.setOnClickListener(ocl);
        return convertView;
    }
    @Override
    public boolean areAllItemsEnabled()
    {
        return true;
    }

    @Override
    public boolean isEnabled(int position)
    {
        return true;
    }

    private static class ThumbnailTask extends AsyncTask<String,Integer,Bitmap> {
        private int mPosition;
        private ViewHolder mHolder;

        public ThumbnailTask(int position, ViewHolder holder) {
            mPosition = position;
            mHolder = holder;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (mHolder.position == mPosition) {
                mHolder.thumbnail.setImageBitmap(bitmap);
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmapOriginal = BitmapFactory.decodeFile(params[0]);
            Bitmap bitmapsimplesize=ThumbnailUtils.extractThumbnail(bitmapOriginal,240,240);
            bitmapOriginal.recycle();
            return bitmapsimplesize;
        }
    }
    private static class ViewHolder {
    public ImageView thumbnail;
    public TextView firstLine;
    public TextView secondLine;
    public int position;
    }
}