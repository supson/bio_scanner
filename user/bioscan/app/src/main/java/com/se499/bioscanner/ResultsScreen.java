package com.se499.bioscanner;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.se499.bioscanner.HomeScreen;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ResultsScreen extends ActionBarActivity {

    private static final String APPLICATION_NAME = "bioscanner";
    public static final String RESULT_POSITION= "result_position";
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_screen);
        List<String> values=new ArrayList<String>();

        db=openOrCreateDatabase("BioscanDB", Context.MODE_PRIVATE, null);
        //get all files in dir
        ArrayList<File> filess=new ArrayList<File>();
        File[] files=getDirectoryListing();
        for(File z : files) {
            filess.add(z);
        }
        ListView resultsList=(ListView) findViewById(R.id.resultsListView);
        ResListEntry[] rList=new ResListEntry[files.length];
        for(int i=0;i<files.length;i++){
            Cursor resultSet = db.rawQuery("Select * from Results WHERE filename='"+files[i].getName()+"'",null);
            resultSet.moveToFirst();
            rList[i]=new ResListEntry();
            rList[i].f=filess.get(i);
            rList[i].isBad=resultSet.getInt(resultSet.getColumnIndex("isBad"));
        }
        ResultsAdapter adapter=new ResultsAdapter(this,rList);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_1, values);
        */
        resultsList.setAdapter(adapter);

    }

    public void selectEntry(View view)
    {
        Intent intent=new Intent(this, DetailResultActivity.class);
        startActivity(intent);
    }

    static public File[] getDirectoryListing()
    {
        File mediaDir=HomeScreen.getAppStorageDir();
        return mediaDir.listFiles();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.action_search){
            Intent intent=new Intent(this, HomeScreen.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        ListView resultsList=(ListView) findViewById(R.id.resultsListView);
    }
}
