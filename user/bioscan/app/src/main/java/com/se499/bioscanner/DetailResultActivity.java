package com.se499.bioscanner;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.zxing.BarcodeFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class DetailResultActivity extends ActionBarActivity implements ActionBar.OnNavigationListener {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * current dropdown position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private File[] entries;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_result);
        Intent intent=getIntent();
        // Set up the action bar to show a dropdown list.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        // Show the Up button in the action bar.
        actionBar.setDisplayHomeAsUpEnabled(true);
        this.entries = ResultsScreen.getDirectoryListing();
        String[] temp = new String[entries.length];
        for (int i = 0; i < entries.length; i++) {
            temp[i] = entries[i].getName();
        }

        // Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(
                // Specify a SpinnerAdapter to populate the dropdown list.
                new ArrayAdapter<String>(
                        actionBar.getThemedContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        temp
                ),
                this);

        db=openOrCreateDatabase("BioscanDB", Context.MODE_PRIVATE, null);
        boolean isNewScan=intent.getBooleanExtra(CameraActivity.NEW_SCAN,false);
        if(isNewScan && savedInstanceState==null) {

            String bacteria1=intent.getStringExtra("b1");
            String bacteria2=intent.getStringExtra("b2");
            double prob1=intent.getDoubleExtra("p1",0.0);
            double prob2=intent.getDoubleExtra("p2", 0.0);
            int id=intent.getIntExtra("id",0);
           // new ApiCallTask(this).execute(img1b64str,img2b64str);
            PlaceholderFragment.bacteria1Name=bacteria1;
            PlaceholderFragment.bacteria2Name=bacteria2;
            PlaceholderFragment.prob1=prob1;
            PlaceholderFragment.prob2=prob2;
            PlaceholderFragment.resId=id;
            actionBar.setSelectedNavigationItem(this.entries.length - 1);
            String filename=this.entries[this.entries.length-1].getName();
            boolean bacteriaDetected=(!PlaceholderFragment.bacteria1Name.equals("Saline") || !PlaceholderFragment.bacteria2Name.equals("Saline"));
            db.execSQL("CREATE TABLE IF NOT EXISTS Results(resultId integer primary key,filename VARCHAR,bacteria1 VARCHAR,bacteria2 VARCHAR,prob1 DOUBLE,prob2 DOUBLE,isBad integer);");
            if(bacteriaDetected) {
                db.execSQL("INSERT INTO Results VALUES(" + id + ",'" + filename + "','"+bacteria1+"','"+bacteria2+"',"+prob1+","+prob2+"," + 1 + ");");
            }
            else {
                db.execSQL("INSERT INTO Results VALUES(" + id + ",'" + filename + "','"+bacteria1+"','"+bacteria2+"',"+prob1+","+prob2+"," + 0 + ");");
            }
        }
        else {
            int selectedPosition = intent.getIntExtra(ResultsScreen.RESULT_POSITION, 0);
            Cursor resultSet = db.rawQuery("Select * from Results WHERE filename='"+entries[selectedPosition].getName()+"'",null);
            resultSet.moveToFirst();
            PlaceholderFragment.bacteria1Name = resultSet.getString(resultSet.getColumnIndex("bacteria1"));
            PlaceholderFragment.bacteria2Name = resultSet.getString(resultSet.getColumnIndex("bacteria2"));
            PlaceholderFragment.prob1=resultSet.getDouble(resultSet.getColumnIndex("prob1"));
            PlaceholderFragment.prob2=resultSet.getDouble(resultSet.getColumnIndex("prob2"));
            PlaceholderFragment.resId=resultSet.getInt(resultSet.getColumnIndex("resultId"));

            actionBar.setSelectedNavigationItem(selectedPosition);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore the previously serialized current dropdown position.
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getSupportActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Serialize the current dropdown position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                getSupportActionBar().getSelectedNavigationIndex());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.action_search){
            Intent intent=new Intent(this, HomeScreen.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(int position, long id) {
        // When the given dropdown item is selected, show its contents in the
        // container view.
        db=openOrCreateDatabase("BioscanDB", Context.MODE_PRIVATE, null);
        Cursor resultSet = db.rawQuery("Select * from Results WHERE filename='"+entries[position].getName()+"'",null);
        resultSet.moveToFirst();
        PlaceholderFragment.bacteria1Name = resultSet.getString(resultSet.getColumnIndex("bacteria1"));
        PlaceholderFragment.bacteria2Name = resultSet.getString(resultSet.getColumnIndex("bacteria2"));
        PlaceholderFragment.prob1=resultSet.getDouble(resultSet.getColumnIndex("prob1"));
        PlaceholderFragment.prob2=resultSet.getDouble(resultSet.getColumnIndex("prob2"));
        PlaceholderFragment.resId=resultSet.getInt(resultSet.getColumnIndex("resultId"));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1,entries[position]))
                .commit();
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public static String bacteria2Name="";
        public static String bacteria1Name="";
        public static double prob1=0;
        public static double prob2=0;
        public static int resId=0;
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_IMAGE_FILE= "image_file";
        private static final String ARG_RESULT_ID="result_id";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber,File img) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString(ARG_IMAGE_FILE,img.getAbsolutePath());
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        public void generateQRButtonOnclick(View view)
        {
            //Intent intent=new Intent(this, ResultsScreen.class);
            //startActivity(intent);
        }
        public void mapsButtonOnClick(View view)
        {
            // Search for restaurants nearby
            Uri gmmIntentUri = Uri.parse("geo:0,0?q=optometrist");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_detail_result, container, false);
            TextView test = (TextView) rootView.findViewById(R.id.detailedTopLine);
            TextView statusLine= (TextView) rootView.findViewById(R.id.detailedStatusTextView);
            Button generateQRbutton=(Button)rootView.findViewById(R.id.detailedQRButton);
            final Button mapsButton=(Button)rootView.findViewById(R.id.detailedMapsButton);
            ImageView thumb=(ImageView)rootView.findViewById(R.id.detailedResultThumb);
            int selectedIndex=getArguments().getInt(ARG_SECTION_NUMBER,0);
            //test.setText(Integer.toString(selectedIndex));
            //test.setText(PlaceholderFragment.bacteria1Name +": "+PlaceholderFragment.prob1+"\n"+ PlaceholderFragment.bacteria2Name+": "+PlaceholderFragment.prob2);
            Bitmap bitmapOriginal = BitmapFactory.decodeFile(getArguments().getString(ARG_IMAGE_FILE));
            Bitmap bitmapsimplesize=ThumbnailUtils.extractThumbnail(bitmapOriginal,240,240);
            thumb.setImageBitmap(bitmapsimplesize);
            bitmapOriginal.recycle();

            mapsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mapsButtonOnClick(v);
                }
            });
            TextView myText = (TextView) rootView.findViewById(R.id.detailedResultAlert);

            boolean bacteriaDetected=(!PlaceholderFragment.bacteria1Name.equals("Saline") || !PlaceholderFragment.bacteria2Name.equals("Saline"));
               if(bacteriaDetected) {
                   QRCodeEncoder qrCodeEncoder = new QRCodeEncoder("http://45.55.159.42:8000/imgscan/retreive/"+(PlaceholderFragment.resId-1),
                           null,
                           Contents.Type.TEXT,
                           BarcodeFormat.QR_CODE.toString(),
                           320);
                   try {
                       Bitmap b = qrCodeEncoder.encodeAsBitmap();
                       thumb.setImageBitmap(b);
                   }catch (Exception e){}
                   myText.setBackgroundColor(Color.RED);
                   myText.setText("BACTERIA DETECTED");
                   Animation anim = new AlphaAnimation(0.0f, 1.0f);
                   anim.setDuration(50); //You can manage the blinking time with this parameter
                   anim.setStartOffset(20);
                   anim.setRepeatMode(Animation.REVERSE);
                   anim.setRepeatCount(Animation.INFINITE);
                   myText.startAnimation(anim);
                   test.setText("Your eyes may be at risk!");
                   statusLine.setText("We recommend you contact your pathologist immediately\n Show them this QR code:");
                   mapsButton.setVisibility(View.INVISIBLE);
                   generateQRbutton.setVisibility(View.INVISIBLE);
               }
                else {
                   myText.setBackgroundColor(Color.GREEN);
                   test.setText("");
                   statusLine.setText("No action nessessary");
                   myText.setText("No bacteria detected");
                   mapsButton.setVisibility(View.GONE);
                   generateQRbutton.setVisibility(View.GONE);
                }
            return rootView;
        }
    }

}
