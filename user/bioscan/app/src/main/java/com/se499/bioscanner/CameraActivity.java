package com.se499.bioscanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;

import java.io.File;
import java.io.InputStream;

import static org.opencv.core.Core.addWeighted;
import static org.opencv.core.Core.circle;
import static org.opencv.core.Core.rectangle;
import static org.opencv.highgui.Highgui.imencode;
import static org.opencv.highgui.Highgui.imwrite;
import static org.opencv.imgproc.Imgproc.BORDER_DEFAULT;
import static org.opencv.imgproc.Imgproc.COLOR_BGR2RGB;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2BGR;
import static org.opencv.imgproc.Imgproc.COLOR_RGBA2BGR;
import static org.opencv.imgproc.Imgproc.CV_HOUGH_GRADIENT;
import static org.opencv.imgproc.Imgproc.Canny;
import static org.opencv.imgproc.Imgproc.GaussianBlur;
import static org.opencv.imgproc.Imgproc.HoughCircles;
import static org.opencv.imgproc.Imgproc.Laplacian;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.cvtColor;
import static org.opencv.imgproc.Imgproc.getRotationMatrix2D;
import static org.opencv.imgproc.Imgproc.medianBlur;
import static org.opencv.imgproc.Imgproc.threshold;
import static org.opencv.imgproc.Imgproc.warpAffine;


public class CameraActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private CameraBridgeViewBase mOpenCvCameraView;
    private String writePath;
    private Boolean sentReq;
    public static String NEW_SCAN="NEW_SCAN";
     @Override
     public void onCreate(Bundle savedInstanceState) {
         Log.i("cv", "called onCreate");
         super.onCreate(savedInstanceState);
         Intent intent=getIntent();
         sentReq=false;
         getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
         setContentView(R.layout.activity_camera);
         mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
         mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
         mOpenCvCameraView.setCvCameraViewListener(this);
         //mOpenCvCameraView.setMaxFrameSize(640,640);
         mOpenCvCameraView.setFocusable(true);

     }

     @Override
     public void onPause()
     {
         super.onPause();
         if (mOpenCvCameraView != null)
             mOpenCvCameraView.disableView();
     }

     public void onDestroy() {
         super.onDestroy();
         if (mOpenCvCameraView != null)
             mOpenCvCameraView.disableView();
     }

     public void onCameraViewStarted(int width, int height) {
     }

     public void onCameraViewStopped() {
     }

     public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
         Mat modMat=new Mat();
         modMat=inputFrame.gray().clone();
         Mat dispMat=new Mat();
         dispMat=inputFrame.rgba().clone();
         rectangle(dispMat, new Point(dispMat.width() * 2.5 / 8, dispMat.height() / 8), new Point(dispMat.width() * 5.5 / 8, dispMat.height() * 7 / 8), new Scalar(0, 0, 0));
         //rotation
         Point src_center = new Point(modMat.cols() / 2.0F, modMat.rows() / 2.0F);
         Mat rot = getRotationMatrix2D(src_center, -90, 1.0);
         warpAffine(modMat, modMat, rot, modMat.size());
         warpAffine(dispMat, dispMat, rot, modMat.size());
         //medianBlur(modMat,modMat,25);

         //circles
         Mat circles=new Mat();
         Mat maskedMat=new Mat(modMat,new Rect(modMat.width()/4 ,modMat.height()/8,modMat.width()/2,modMat.height()/2+modMat.height()/4));
         //HoughCircles(maskedMat,circles,CV_HOUGH_GRADIENT,1,modMat.rows()/8,100,100,modMat.width()/16,modMat.width()/2);
         HoughCircles(maskedMat,circles,CV_HOUGH_GRADIENT,1,modMat.rows()/8,80,60,modMat.width()/16,0);

         //draw circles
         for(int i=0;i<Math.min(2,circles.cols());i++) {
                double[] c = circles.get(0, i);
                //circle(dispMat, new Point(c[0]+modMat.width()/4, c[1]+modMat.height()/8), (int)c[2], new Scalar(0, 0, 255),1,8,0);
         }

         if(circles.cols()==2 && !sentReq ){
                 writePath=HomeScreen.getOutputMediaFileUri().toString();
                 File x=new File(Uri.parse(this.writePath).getPath());
                 cvtColor(dispMat,dispMat,COLOR_RGBA2BGR);
                 imwrite(x.toString(), dispMat);

                 //Mat mask1=Mat.zeros(dispMat.rows(),dispMat.cols(),CvType.CV_8U);
                 //circle(mask1, new Point(circles.get(0, 0)[0], circles.get(0, 0)[1]), (int) (circles.get(0, 0)[2]), new Scalar(255, 255, 255), -1, 8, 0);
                 //Mat mask2=Mat.zeros(dispMat.rows(),dispMat.cols(),CvType.CV_8U);
                 //circle(mask2, new Point(circles.get(0, 1)[0], circles.get(0, 1)[1]), (int) (circles.get(0, 1)[2]), new Scalar(255, 255, 255), -1, 8, 0);

                 Mat temp=new Mat(dispMat,new Rect((int)(circles.get(0,0)[0]-circles.get(0,0)[2]+modMat.width()/4),(int)(circles.get(0,0)[1]-circles.get(0,0)[2]+modMat.height()/8),(int)(circles.get(0,0)[2]*2),(int)(circles.get(0,0)[2]*2)));
                 Mat temp2 =new Mat(dispMat,new Rect((int)(circles.get(0,1)[0]-circles.get(0,1)[2]+modMat.width()/4),(int)(circles.get(0,1)[1]-circles.get(0,1)[2]+modMat.height()/8),(int)(circles.get(0,1)[2]*2),(int)(circles.get(0,1)[2]*2)));
                 temp2.convertTo(temp2,CvType.CV_32F);

                 MatOfInt params90=new MatOfInt(Highgui.IMWRITE_JPEG_QUALITY,100);
                 MatOfByte buff1 = new MatOfByte();
                 MatOfByte buff2= new MatOfByte();
                 imencode(".jpg",temp,buff1,params90);
                 imencode(".jpg",temp2,buff2,params90);
                 String img1b64str;
                 String img2b64str;
                 if(circles.get(0,0)[0]<circles.get(0,1)[0]) {
                     img1b64str = new String(Base64.encode(buff1.toArray(), Base64.DEFAULT));
                     img2b64str = new String(Base64.encode(buff2.toArray(), Base64.DEFAULT));
                 }else{
                     img2b64str = new String(Base64.encode(buff1.toArray(), Base64.DEFAULT));
                     img1b64str = new String(Base64.encode(buff2.toArray(), Base64.DEFAULT));
                 }
                 sentReq=true;
                 new ApiCallTask(this).execute(img1b64str,img2b64str);


         }
         return dispMat;
     }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("cv", "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        this.sentReq=false;
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mLoaderCallback);
    }
    class ApiCallTask extends AsyncTask<String, Void, JSONObject> {

        private Exception exception;
        private ProgressDialog dialog;
        private Context context;

        public ApiCallTask(Context c){
            context=c;
        }
        public String convertStreamToString(java.io.InputStream is){
            java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
            return s.hasNext() ? s.next():"";
        }

        @Override
        protected  void onPreExecute(){
            /*dialog = new ProgressDialog(context);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Loading. Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();*/
        }

        protected JSONObject doInBackground(String... imgs) {
            JSONObject jsonObjrec=new JSONObject();
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost("http://45.55.159.42:8000/imgscan/process/");
                post.setHeader("Accept", "application/json");
                post.setHeader("Content-Type", "application/json");
                JSONObject jobj=new JSONObject();
                jobj.put("img1",imgs[0]);
                jobj.put("img2", imgs[1]);
                jobj.put("phone", "nexus5");
                post.setEntity(new StringEntity(jobj.toString()));
                HttpResponse rsp=client.execute(post);
                HttpEntity entity=rsp.getEntity();
                if(entity!=null){
                    InputStream inputStream=entity.getContent();
                    String resultStr=convertStreamToString(inputStream);
                    inputStream.close();
                    jsonObjrec=new JSONObject(resultStr);
                }
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
            return jsonObjrec;
        }

        protected void onPostExecute(JSONObject rsp) {

            try {
                String t= rsp.get("bacteria1").toString();
                String t2= rsp.get("bacteria2").toString();
                double p1=rsp.getDouble("probability1");
                double p2=rsp.getDouble("probability2");
                int id=rsp.getInt("id");
                Intent intent2=new Intent(context, DetailResultActivity.class);
                intent2.putExtra(NEW_SCAN,true);
                intent2.putExtra("b1",t);
                intent2.putExtra("b2", t2);
                intent2.putExtra("p1",p1);
                intent2.putExtra("p2",p2);
                intent2.putExtra("id",id);
                startActivity(intent2);
            }catch(Exception e) {
            }
            //dialog.dismiss();
        }
    }
}


