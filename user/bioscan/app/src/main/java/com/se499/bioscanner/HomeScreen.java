package com.se499.bioscanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class HomeScreen extends Activity{
    private Uri photoUri;
    private static final int CAPTURE_ACTIVITY_REQUEST_CODE = 100;
    private static final int SELECT_ACTIVITY_REQUEST_CODE = 200;
    private static final String APPLICATION_NAME = "bioscanner";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void resultsButton(View view)
    {
        Intent intent=new Intent(this, ResultsScreen.class);
        startActivity(intent);
    }

    /** called when the user clicks the scan button */
    public void scanButton(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        photoUri = HomeScreen.getOutputMediaFileUri();
        if (photoUri == null) {
            Toast.makeText(
                    this, R.string.cannot_write_to_external_storage,
                    Toast.LENGTH_LONG).show();
            return;
        }
        final Intent intent2=new Intent(this, CameraActivity.class);
        intent2.putExtra("IMAGE_PATH", photoUri.toString());
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Steps:\n1.Place case on white sheet of paper\n2. Position phone 2 inches away from case\n3.Ensure that the case is within the black rectangle\n4.Slowly lift phone away from case\n");
        alertDialogBuilder.setPositiveButton("Scan",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        startActivity(intent2);

                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        //startActivityForResult(intent, CAPTURE_ACTIVITY_REQUEST_CODE);

        return;

    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        if(requestCode==CAPTURE_ACTIVITY_REQUEST_CODE) {
            if(resultCode==RESULT_OK) {
                /*Toast.makeText(
                        this, "image got",
                        Toast.LENGTH_LONG).show();*/
                Intent intent=new Intent(this,DetailResultActivity.class);
                intent.putExtra(ResultsScreen.RESULT_POSITION,ResultsScreen.getDirectoryListing().length-1);
                this.startActivity(intent);

            }
        }

    }
    /** Create a file Uri for saving an image */
    public static Uri getOutputMediaFileUri(){
        try {
            return Uri.fromFile(getOutputMediaFile());
        } catch(NullPointerException e) {
            return null;
        }
    }

    public static File getAppStorageDir()
    {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), APPLICATION_NAME);

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.e(APPLICATION_NAME, "failed to create directory");
                return null;
            }
        }
        return mediaStorageDir;
    }
    /** Create a File for saving an image */
    private static File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = getAppStorageDir();

        // Create a media file pseudo random name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).
                format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");

        return mediaFile;
    }

    public void sendNotification(View view){
        Intent intent = new Intent(this, HomeScreen.class);
    PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

    // build notification
    // the addAction re-use the same intent to keep the example short
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle("BioScanner notification");
        builder.setContentText("10 days overdue!");
        builder.setSmallIcon(R.drawable.icon);
        builder.setContentIntent(pIntent);
        builder.setAutoCancel(true);
        builder.setLights(Color.RED, 3000, 3000);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
builder.setSound(alarmSound);
        Notification n = builder.getNotification();
        n.defaults|=Notification.DEFAULT_ALL;


        NotificationManager notificationManager =
          (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
    }


}
