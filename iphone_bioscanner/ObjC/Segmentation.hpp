//
//  Segmentation.hpp
//  BioScanner
//
//  Created by Raian Huq on 2015-03-28.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Segmentation : NSObject
+ (UIImage* __strong *) detect_lenses:(UIImage*) image num_circles:(int) num_circles;
+(void) dalloc:(UIImage* __strong *) cls;
+(void) reset_detection;
@end
