//
//  Segmentation.mm
//  BioScanner
//
//  Created by Raian Huq on 2015-03-28.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

#import "Segmentation.hpp"
#import <opencv2/opencv.hpp>
#import <CoreImage/CoreImage.h>

using namespace cv;
@implementation Segmentation
static int const EDGE_DETECTION_MAX = 80;
static int const EDGE_DETECTION_MIN = 40;
static int const EDGE_INCREMENT = 5;
static int const EDGE_DETECTION_START_VAL = 80;

static int const CENTER_DETECTION_MAX = 60;
static int const CENTER_DETECTION_MIN = 35;
static int const CENTER_INCREMENT = 5;
static int const CENTER_DETECTION_START_VAL = 60;

static int edge_detection = EDGE_DETECTION_START_VAL;
static int center_detection = CENTER_DETECTION_START_VAL;

+(void) reset_detection{
    edge_detection = EDGE_DETECTION_START_VAL;
    center_detection = CENTER_DETECTION_START_VAL;
}

+ (UIImage* __strong *) detect_lenses:(UIImage*) image
                        num_circles:(int) num_circles{
    int ksize = 33;
    int bitdepth = CV_8U;
    float divisor = 6;
    float len_fac = 1 - (2/divisor);

    CGFloat height = image.size.width;
    CGFloat width = image.size.height;

    CGRect roi = CGRectMake(width/divisor, height/divisor, width * len_fac, height * len_fac);
    image = crop_to_rect(image, roi);
//    printf("image width:%d, image height:%d\n", (int)image.size.width, (int)image.size.height);
    cv::Mat input_mat = cvMatFromUIImage(image);
    cv::Mat gray_mat;
    cv::cvtColor(input_mat, gray_mat, CV_BGR2GRAY);
    
    cv::Mat gray_blurred_mat;
    cv::medianBlur(gray_mat, gray_blurred_mat, ksize);
    
//    cv::Mat laplacian_mat;
//    cv::Laplacian(gray_blurred_mat, laplacian_mat, bitdepth);
//    cv::Mat sharpened_mat;
//    cv::add(gray_blurred_mat, laplacian_mat, sharpened_mat);
    
    vector<cv::Vec3f> circles;
    CGFloat min_radius = 200, max_radius = 400;
//    CGFloat min_radius = 0, max_radius = 0;

    printf("current edge detection: %d\n", edge_detection);
    HoughCircles(gray_blurred_mat, circles, CV_HOUGH_GRADIENT, 1, gray_blurred_mat.rows/8, edge_detection, center_detection, min_radius, max_radius);

    printf("%d\n", (int)circles.size());
    if(circles.size() > num_circles){
        if(edge_detection + EDGE_INCREMENT < EDGE_DETECTION_MAX)
            edge_detection += EDGE_INCREMENT;
        if(center_detection + CENTER_INCREMENT < CENTER_DETECTION_MAX)
            center_detection += CENTER_INCREMENT;
        return NULL;
    }else if(circles.size() < num_circles){
        if(edge_detection - EDGE_INCREMENT > EDGE_DETECTION_MIN)
            edge_detection -= EDGE_INCREMENT;
        if(center_detection - CENTER_INCREMENT > CENTER_DETECTION_MIN)
            center_detection -= CENTER_INCREMENT;
        return NULL;
    }
    [Segmentation reset_detection];
    
    Vec3i l, r;
    UIImage* __strong * cls = new UIImage* [num_circles]();
    
    if(num_circles == 2){
        printf("zero index x: %f\n", circles[0][0]);
        printf("second index x: %f\n", circles[1][0]);
        //&& [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait)
        if( (circles[0][1] > circles[1][1] && [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait)
           || (circles[0][0] < circles[1][0] && [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft)
           || (circles[0][1] < circles[1][1] && [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown)){
            printf("%s\n", "asdf");
            l = circles[0];
            r = circles[1];
        }else{
            printf("%s\n", "2nd");
            l = circles[1];
            r = circles[0];
        }
        
        //not sure why radius has to be subtracted from Y value instead of added
        CGRect left_rect = CGRectMake(l[0] - l[2], l[1] - l[2], 2 * l[2], 2 * l[2]);
        CGRect right_rect = CGRectMake(r[0] - r[2], r[1] - r[2], 2 * r[2], 2 * r[2]);
        cls[0] = crop_to_rect(image, left_rect);
        cls[1] = crop_to_rect(image, right_rect);
    }else if(num_circles == 1){
        Vec3i c = circles[0];
        CGRect circle_rect = CGRectMake(c[0] - c[2], c[1] - c[2], 2 * c[2], 2 * c[2]);
        cls[0] = crop_to_rect(image, circle_rect);
    }
    return cls;
}

+(void) dalloc:(UIImage* __strong *) cls{
    if(cls != NULL){
        delete [] cls;
        cls = NULL;
    }
}

static UIImage* crop_to_rect(UIImage* imageToCrop, CGRect rect){
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return cropped;
}

static Mat cvMatFromUIImage(UIImage *image){
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    // added this if statement
    if  (image.imageOrientation == UIImageOrientationLeft || image.imageOrientation == UIImageOrientationRight) {
        cols = image.size.height;
        rows = image.size.width;
    }

    Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

static UIImage* UIImageFromCVMat(Mat cvMat){
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

static inline double radians (double degrees) {return degrees * M_PI/180;}
@end

/*
 for(size_t i = 0; i < circles.size(); i++){
 Vec3i c = circles[i];
 printf("radius: %d\n", c[2]);
 circle(input_mat, cv::Point(c[0], c[1]), c[2], Scalar(255,0,0), 3, CV_AA);
 circle(input_mat, cv::Point(c[0], c[1]), 2, Scalar(0,255,0), 3, CV_AA);
 
 //not sure why radius has to be subtracted from Y value instead of added
 CGRect lense_rect = CGRectMake(c[0] - c[2], c[1] - c[2], 2 * c[2], 2 * c[2]);
 cls[i] = crop_to_rect(image, lense_rect);
 }
*/

/* saving to Document directory
 //Creating Path to Documents-Directory
 NSData *pngData = UIImagePNGRepresentation(ret);
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
 NSString *filePath = [documentsPath stringByAppendingPathComponent:@"image.png"]; //Add the file name
 [pngData writeToFile:filePath atomically:YES]; //Write the file
 */
