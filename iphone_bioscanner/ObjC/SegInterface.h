//
//  SegInterface.h
//  BioScanner
//
//  Created by Raian Huq on 2015-03-29.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SegInterface : NSObject
+ (UIImage* __strong *) detectLenses:(UIImage*) image num_circles:(int) num_circles;
+(void) dalloc:(UIImage* __strong *) cls;
+(void) resetDetection;
@end
