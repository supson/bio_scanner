//
//  SegInterface.m
//  BioScanner
//
//  Created by Raian Huq on 2015-03-29.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

#import "SegInterface.h"
#import "Segmentation.hpp"

@implementation SegInterface
+ (UIImage* __strong *) detectLenses:(UIImage*) image num_circles:(int) num_circles{
    return [Segmentation detect_lenses:image num_circles:num_circles];
}

+(void) dalloc:(UIImage* __strong *) cls{
    return [Segmentation dalloc:cls];
}

+(void) resetDetection{
    [Segmentation reset_detection];
}
@end
