//
//  vcQR.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-04-12.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import Foundation
import MobileCoreServices
import AVFoundation
import CoreData

//implemented with http://www.appcoda.com/qr-code-reader-swift/
class vcQR: ViewControllerNoRotate, AVCaptureMetadataOutputObjectsDelegate {
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var scanComplete: Bool = false
    
    override func viewDidLoad() {
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        var error: NSError?
        let input: AnyObject! = AVCaptureDeviceInput.deviceInputWithDevice(captureDevice, error: &error)
        
        if (error != nil) {
            // If any error occurs, simply log the description of it and don't continue any more.
            println("\(error?.localizedDescription)")
            return
        }
        
        // Initialize the captureSession object.
        captureSession = AVCaptureSession()
        // Set the input device on the capture session.
        captureSession?.addInput(input as! AVCaptureInput)
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        // Set self as delegate and use the default dispatch queue to execute the call back
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer)
        
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        qrCodeFrameView?.layer.borderColor = UIColor.greenColor().CGColor
        qrCodeFrameView?.layer.borderWidth = 2
        view.addSubview(qrCodeFrameView!)
        view.bringSubviewToFront(qrCodeFrameView!)
        
        // Start video capture.
        captureSession?.startRunning()
        
    }//end of viewDidLoad

    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            //no QR code detected
            qrCodeFrameView?.frame = CGRectZero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrCodeFrameView?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                if scanComplete || !(metadataObj.stringValue as NSString!).containsString(SERVER_IP) {
                    return
                }
                
                scanComplete = true
                print("QR Code: ")
                println(metadataObj.stringValue)
                
                var errorPointer: NSErrorPointer = nil
                let returnData = util.apiGetRequest(metadataObj.stringValue)
                if(returnData == nil){
                    return
                }
                
                let responseDict = NSJSONSerialization.JSONObjectWithData(returnData!, options: NSJSONReadingOptions.MutableLeaves, error: errorPointer) as! NSDictionary
                
                let newEntity = NSEntityDescription.insertNewObjectForEntityForName("ImageEntity", inManagedObjectContext: gManagedObjectContext) as! ImageEntity
                
                newEntity.detected = (responseDict["ok"]?.integerValue == 1)
                newEntity.probability1 = responseDict["probability1"]!.floatValue
                newEntity.probability2 = responseDict["probability2"]!.floatValue
                newEntity.bacteriaName1 = responseDict["bacteria1"] as! String
                newEntity.bacteriaName2 = responseDict["bacteria2"] as! String
//                newEntity.imageData = jpeg
                let formatter = NSDateFormatter()
                formatter.dateStyle = NSDateFormatterStyle.LongStyle
                newEntity.date = formatter.stringFromDate(NSDate())
                if !(gManagedObjectContext.save(nil)) {
                    println("cannot save new Image Entity")
                    abort()
                }
                
                println("showing result screen...")
                let resultScreen = gStoryboard.instantiateViewControllerWithIdentifier("resultScreen") as? vcResult
                resultScreen!.initialize(nil,
                    probability1: newEntity.probability1,
                    probability2: newEntity.probability2,
                    detected: newEntity.detected,
                    bacteriaName1: newEntity.bacteriaName1,
                    bacteriaName2: newEntity.bacteriaName2)
                self.navigationController?.popToRootViewControllerAnimated(true)                
                self.navigationController?.showViewController(resultScreen!, sender: nil)
            }
        }
    }//end of captureOutput
    
    func setFrameOnOrientationChange(){
        videoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: util.screenSize().width, height: util.screenSize().height)
    }
    
//    override func shouldAutorotate() -> Bool {
//        setFrameOnOrientationChange()
//        return true;
//    }
    
    override func viewDidLayoutSubviews() {
        videoPreviewLayer?.connection.videoOrientation = videoOrientationFromCurrentDeviceOrientation()
        setFrameOnOrientationChange()
    }
    
    func videoOrientationFromCurrentDeviceOrientation() -> AVCaptureVideoOrientation {
        switch (UIDevice.currentDevice().orientation) {
        case UIDeviceOrientation.Portrait:
            return AVCaptureVideoOrientation.Portrait;
        case UIDeviceOrientation.LandscapeLeft:
            return AVCaptureVideoOrientation.LandscapeRight;
        case UIDeviceOrientation.LandscapeRight:
            return AVCaptureVideoOrientation.LandscapeLeft;
        case UIDeviceOrientation.PortraitUpsideDown:
            return AVCaptureVideoOrientation.PortraitUpsideDown;
        default:
            return AVCaptureVideoOrientation.Portrait;
        }
    }
}
