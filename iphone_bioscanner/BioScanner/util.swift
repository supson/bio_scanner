//
//  util.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-23.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import Foundation
import UIKit

//NOTE THAT FOR SWIFT VERSION 1.2 OR UP (XCODE 6.3 OR UP) YOU NEED TO ADD "!" AFTER THE WORD "as"
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
let gManagedObjectContext = ((UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext)!
let gStoryboard: UIStoryboard! = UIStoryboard(name: "Storyboard", bundle: nil)

let SALINE = "Saline"
let STAPHYLOCOCCUS_AUREUS = "Staphylococcus_aureus"
let ACHROMOBACTER_XYLOSOXIDANS = "Achromobacter_xylosoxidans"
let SERRATIA_MARCESCENS = "Serratia_marcescens"
let DELFTIA_ACIDOVORANS = "Delftia_acidovorans"
let STENOTROPHOMONAS_MALTOPHILIA = "Stenotrophomonas_maltophilia"
let PSEUDOMONAS_AERUGINOSA = "Pseudomonas_aeruginosa"

let bacteriaDict = [SALINE: "Saline",
    STAPHYLOCOCCUS_AUREUS: "Staphylococcus aureus",
    ACHROMOBACTER_XYLOSOXIDANS: "Achromobacter xylosoxidans",
    SERRATIA_MARCESCENS: "Serratia marcescens",
    DELFTIA_ACIDOVORANS: "Delftia acidovorans",
    STENOTROPHOMONAS_MALTOPHILIA: "Stenotrophomonas maltophilia",
    PSEUDOMONAS_AERUGINOSA: "Pseudomonas aeruginosa"
    ] as Dictionary<String, String?>

let BASE_URL = "http://45.55.159.42:8000/imgscan/"
let PROCESS_URL_EXT = "process/"
let TRAIN_URL_EXT = "train/"
let SERVER_IP = "45.55.159.42"

class util{
    
    enum ScanType{
        case DETECT_BACTERIA
        case TRAIN_MODEL
    }
    
    class func screenSize() -> CGSize {
        let screenSize = UIScreen.mainScreen().bounds.size
        if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) {
            return CGSizeMake(screenSize.height, screenSize.width)
        }
        return screenSize
    }
    
    class func deviceSize() -> CGSize{
        var size = UIScreen.mainScreen().bounds.size
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft ||
            UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeRight{
                var temp = size.height
                size.height = size.width
                size.width = temp
        }
        return size
    }
    
    class func apiPostRequestProcess(callType: ScanType, leftLenseString: NSString!, rightLenseString: NSString!) -> NSData?{
        var returnData: NSData?
        if(callType == ScanType.DETECT_BACTERIA){
            let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL + PROCESS_URL_EXT)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
            var response: NSURLResponse?
            var errorPointer: NSErrorPointer = nil
        
            let jsonDict = ["img1": leftLenseString, "img2": rightLenseString]
            let jsonString = NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions.allZeros, error: errorPointer)
            request.HTTPMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.HTTPBody = jsonString
            returnData = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: errorPointer)
            if(returnData == nil){
                return nil
            }
            println("HTTP response: \((response as! NSHTTPURLResponse).statusCode)")
        }
        return returnData
    }
    
    class func apiPostRequestTrain(callType: ScanType, lenseString: NSString!, bacteria: NSString!) -> NSData?{
        var returnData: NSData? = nil
        if(callType == ScanType.TRAIN_MODEL){
            let request = NSMutableURLRequest(URL: NSURL(string: BASE_URL + TRAIN_URL_EXT)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
            var response: NSURLResponse?
            var errorPointer: NSErrorPointer = nil
            
            let jsonDict = ["img": lenseString, "bacteria": bacteria]
            let jsonString = NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions.allZeros, error: errorPointer)
            
            request.HTTPMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.HTTPBody = jsonString
            
            returnData = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: errorPointer)
            if(returnData == nil){
                return nil
            }
            println("HTTP response: \((response as! NSHTTPURLResponse).statusCode)")
        }
        return returnData
    }
    
    class func apiGetRequest(get_url: NSString!) -> NSData?{
        var returnData: NSData?
        let request = NSMutableURLRequest(URL: NSURL(string: get_url as String)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
        var response: NSURLResponse?
        var errorPointer: NSErrorPointer = nil
        request.HTTPMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        returnData = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: errorPointer)
        if(returnData == nil){
            return nil
        }
        println("HTTP response: \((response as! NSHTTPURLResponse).statusCode)")
        
        return returnData
    }
}