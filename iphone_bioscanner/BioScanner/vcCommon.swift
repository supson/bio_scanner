//
//  vcCommon.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-04-19.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

extension UINavigationController {
    public override func supportedInterfaceOrientations() -> Int {
        return visibleViewController.supportedInterfaceOrientations()
    }
    public override func shouldAutorotate() -> Bool {
        return visibleViewController.shouldAutorotate()
    }
}

class ViewControllerNoRotate: UIViewController {
    override func supportedInterfaceOrientations() -> Int {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }else{
            return Int(UIInterfaceOrientationMask.Portrait.rawValue)
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
    }
}

class TableViewControllerNoRotate: UITableViewController {
    override func supportedInterfaceOrientations() -> Int {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }else{
            return Int(UIInterfaceOrientationMask.Portrait.rawValue)
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
    }
}

