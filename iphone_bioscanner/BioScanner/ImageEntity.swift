//
//  BioScanner.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-27.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import Foundation
import CoreData

class ImageEntity: NSManagedObject {
    @NSManaged var bacteriaName1: String
    @NSManaged var bacteriaName2: String
    @NSManaged var probability1: Float
    @NSManaged var probability2: Float
    @NSManaged var date: String
    @NSManaged var detected: Bool
    @NSManaged var imageData: NSData

}
