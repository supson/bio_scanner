//
//  vcTrain.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-23.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import UIKit
import MobileCoreServices

class vcTrain: ViewControllerNoRotate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LenseScannerDelegate{
    

    var pictureTaken = false    
    @IBOutlet var bacteriaPicker: UIPickerView!
    var bacteriaNames = [SALINE, STAPHYLOCOCCUS_AUREUS, ACHROMOBACTER_XYLOSOXIDANS, PSEUDOMONAS_AERUGINOSA]
    
    var overlay: UIView!
    var imagePicker: UIImagePickerController!
    var lenseString: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        bacteriaPicker.delegate = self
        bacteriaPicker.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return bacteriaNames.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        return bacteriaDict[bacteriaNames[row]]!
    }

    @IBAction func upload(){
        let currentBacteria = bacteriaNames[bacteriaPicker.selectedRowInComponent(0)]
        var toast: UIAlertController!
        if(!pictureTaken){
            toast = UIAlertController(title: "Error", message: "No picture taken", preferredStyle: UIAlertControllerStyle.Alert)
        }else{
            //handle upload
            util.apiPostRequestTrain(util.ScanType.TRAIN_MODEL, lenseString: self.lenseString, bacteria: currentBacteria)
            toast = UIAlertController(title: "Success", message: "Upload complete", preferredStyle: UIAlertControllerStyle.Alert)
            pictureTaken = false
        }

        toast.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(toast, animated: true, completion: nil)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "vcScanSegue" {
            var lenseScanner = segue.destinationViewController as! vcScan
            lenseScanner.delegate = self
            lenseScanner.detectionType = util.ScanType.TRAIN_MODEL
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]){
        println(info[UIImagePickerControllerOriginalImage])
        //process image
        pictureTaken = true
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func scanFinished(originalImage: UIImage, leftLenseString: NSString?, rightLenseString: NSString?){
        pictureTaken = true
        self.lenseString = leftLenseString
    }
}
