//
//  vcMain.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-21.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreData

class vcMain: ViewControllerNoRotate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LenseScannerDelegate{
    var overlay: UIView!
    var imagePicker: UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "vcScanSegue" {
            var lenseScanner = segue.destinationViewController as! vcScan
            lenseScanner.delegate = self
            lenseScanner.detectionType = util.ScanType.DETECT_BACTERIA
        }
    }
    
    func scanFinished(originalImage: UIImage, leftLenseString: NSString?, rightLenseString: NSString?) {
        var errorPointer: NSErrorPointer = nil
        
        let returnData = util.apiPostRequestProcess(util.ScanType.DETECT_BACTERIA, leftLenseString: leftLenseString, rightLenseString: rightLenseString)
        if(returnData == nil){
            return
        }
        let responseDict = NSJSONSerialization.JSONObjectWithData(returnData!, options: NSJSONReadingOptions.MutableLeaves, error: errorPointer) as! NSDictionary
        println(responseDict);
 
        //save image to CoreData
        var jpeg = UIImageJPEGRepresentation(originalImage as UIImage!, 0.5)
        //exclamation after "as" may be needed
        let newEntity = NSEntityDescription.insertNewObjectForEntityForName("ImageEntity", inManagedObjectContext: gManagedObjectContext) as! ImageEntity
        
        newEntity.detected = (responseDict["ok"]?.integerValue == 1)
        newEntity.probability1 = responseDict["probability1"]!.floatValue
        newEntity.probability2 = responseDict["probability2"]!.floatValue
        newEntity.bacteriaName1 = responseDict["bacteria1"] as! String
        newEntity.bacteriaName2 = responseDict["bacteria2"] as! String
        newEntity.detected = (newEntity.bacteriaName1 != SALINE || newEntity.bacteriaName2 != SALINE)
        
//        newEntity.imageData = jpeg
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.LongStyle
        newEntity.date = formatter.stringFromDate(NSDate())
        if !(gManagedObjectContext.save(nil)) {
            println("cannot save new Image Entity")
            abort()
        }
        
        println("showing result screen...")
        let resultScreen = gStoryboard.instantiateViewControllerWithIdentifier("resultScreen") as? vcResult
        resultScreen!.initialize(nil,
            probability1: newEntity.probability1,
            probability2: newEntity.probability2,
            detected: newEntity.detected,
            bacteriaName1: newEntity.bacteriaName1,
            bacteriaName2: newEntity.bacteriaName2)
        self.navigationController?.showViewController(resultScreen!, sender: nil)
    }
}
