//
//  vcResult.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-24.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import UIKit

class vcResult: ViewControllerNoRotate {
    @IBOutlet var resultMessage: UITextField!
    @IBOutlet var imageView: UIImageView!
    

//    @IBOutlet var bacteria1: UILabel!
//    @IBOutlet var bacteria2: UILabel!
    
    var jpeg: NSData! = nil
    var detected: Bool = false
    var probability1: Float = 50.0
    var probability2: Float = 50.0
    var bacteriaName1: NSString! = "Saline"
    var bacteriaName2: NSString! = "Saline"
    
    @IBOutlet var bacteria1: UITextView!
    @IBOutlet var bacteria2: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if(jpeg != nil){
            imageView.image = UIImage(data: jpeg)
        }
//        var leftBacteriaName: NSString! = bacteriaDict[bacteriaName1 as String]!
//        var rightBacteriaName: NSString! = bacteriaDict[bacteriaName2 as String]!

        var leftBacteriaName: NSString! = bacteriaName1.stringByReplacingOccurrencesOfString("_", withString: " ")
        var rightBacteriaName: NSString! = bacteriaName2.stringByReplacingOccurrencesOfString("_", withString: " ")
        
        bacteria1.text = "\(leftBacteriaName) \((Int)(probability1 * 100))%"
        bacteria2.text = "\(rightBacteriaName) \((Int)(probability2 * 100))%"
        resultMessage.text = (bacteriaName1 != SALINE || bacteriaName2 != SALINE) ? "Warning detected!" : "No warning detected"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func initialize(jpeg: NSData!, probability1: Float, probability2: Float, detected: Bool, bacteriaName1: NSString!, bacteriaName2: NSString!){
        self.bacteriaName1 = bacteriaName1
        self.bacteriaName2 = bacteriaName2
        self.jpeg = jpeg
        self.probability1 = probability1
        self.probability2 = probability2
        self.detected = detected
    }

}
