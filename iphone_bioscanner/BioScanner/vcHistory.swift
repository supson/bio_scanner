//
//  vcHistory.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-24.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import UIKit
import CoreData

class vcHistory: TableViewControllerNoRotate {
    var historyArray: [ImageEntity!]!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let fetchRequest = NSFetchRequest(entityName: "ImageEntity")
        var count = gManagedObjectContext.countForFetchRequest(fetchRequest, error: nil)
        if count == NSNotFound{
            historyArray = nil
            println("FETCH REQUEST NOT FOUND")
            return
        }
        historyArray = gManagedObjectContext.executeFetchRequest(fetchRequest, error: nil) as! [ImageEntity!]!
    }

    override func supportedInterfaceOrientations() -> Int {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad){
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }else{
            return Int(UIInterfaceOrientationMask.Portrait.rawValue)
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "viewResult" {
            var selectedItem = historyArray[self.tableView.indexPathForSelectedRow()!.row]
            var resultScreen = segue.destinationViewController as! vcResult
            
            var msg = (selectedItem.detected) ? "Warning Detected!" : "No warning detected"
            resultScreen.initialize(selectedItem.imageData,
                probability1: selectedItem.probability1,
                probability2: selectedItem.probability2,
                detected: selectedItem.detected,
                bacteriaName1: selectedItem.bacteriaName1,
                bacteriaName2: selectedItem.bacteriaName2)
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (historyArray == nil) ? 0 : historyArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("historyCell", forIndexPath: indexPath) as! UITableViewCell
        let history =  historyArray[indexPath.row] as ImageEntity!
        cell.textLabel?.text = history.date
        cell.detailTextLabel?.text = (history.detected) ? "Detected!" : "No Warning"
        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete{
            gManagedObjectContext.deleteObject(historyArray[indexPath.row])
            historyArray.removeAtIndex(indexPath.row)
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    @IBAction func clearHistoryTapped(){
        var confirmAlert = UIAlertController(title: "Confirm", message: "All data will be cleared", preferredStyle: UIAlertControllerStyle.Alert)
        
        confirmAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Destructive, handler: { (action: UIAlertAction!) in
            //ok button pressed
            self.clearHistory()
        }))
        
        confirmAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(confirmAlert, animated: true, completion: nil)
    }
    
    func clearHistory(){
        let fetchRequest = NSFetchRequest(entityName: "ImageEntity")
        fetchRequest.includesPropertyValues = false
        var allImageEntities = gManagedObjectContext.executeFetchRequest(fetchRequest, error: nil) as! [ImageEntity!]!
        for imageEntity in allImageEntities{
            gManagedObjectContext.deleteObject(imageEntity)
        }
        if !(gManagedObjectContext.save(nil)) {
            println("cannot save new Image Entity")
            abort()
        }
        historyArray = nil
        self.tableView.reloadData()
    }
}
