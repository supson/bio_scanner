//
//  vcCamera.swift
//  BioScanner
//
//  Created by Raian Huq on 2015-03-23.
//  Copyright (c) 2015 Raian Huq. All rights reserved.
//

import Foundation
import MobileCoreServices
import UIKit
import AVFoundation
import QuartzCore
import CoreData

protocol LenseScannerDelegate{
    func scanFinished(originalImage: UIImage, leftLenseString: NSString?, rightLenseString: NSString?);
}

class vcScan: ViewControllerNoRotate{
    private struct dim{
        static var rectHeight: CGFloat! = util.deviceSize().height/4
        static var rectWidth: CGFloat! = util.deviceSize().width/1.5
    }
    
    let captureSession = AVCaptureSession()
    var captureDevice : AVCaptureDevice?
    var previewLayer : AVCaptureVideoPreviewLayer?
    var rectLayer: CALayer!
    var alphaLayer: CAShapeLayer!
    let buttonRatioX : CGFloat = 0.01
    let buttonRatioY : CGFloat = 0.88
    var cameraCancelButton: UIButton!
    let captureStillImageOutput: AVCaptureStillImageOutput = AVCaptureStillImageOutput()
    var cameraTimer: NSTimer?
    var done: Bool = false
    var delegate: LenseScannerDelegate?
    var detectionType: util.ScanType = util.ScanType.DETECT_BACTERIA
    @IBOutlet var instructionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        cameraCancelButton = UIButton(frame: CGRectMake(buttonRatioX * util.screenSize().width, buttonRatioY * util.screenSize().height, util.deviceSize().width/5, util.deviceSize().height/10))
        cameraCancelButton.backgroundColor = UIColor.clearColor()
        cameraCancelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        cameraCancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        cameraCancelButton.layer.borderColor = UIColor.whiteColor().CGColor
        cameraCancelButton.layer.borderWidth = 1.0
        cameraCancelButton.layer.cornerRadius = 10
        cameraCancelButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        cameraCancelButton.addTarget(self, action: "cameraCancelled:", forControlEvents: UIControlEvents.TouchUpInside)
        cameraCancelButton.titleLabel?.font = UIFont.systemFontOfSize(18)
        cameraCancelButton.autoresizingMask = UIViewAutoresizing.None
        self.view.addSubview(cameraCancelButton)

        var rectBezierPath = UIBezierPath(rect: CGRect(x: util.screenSize().width/2 - dim.rectWidth/2, y: util.screenSize().height/2 - dim.rectHeight/2, width: dim.rectWidth, height: dim.rectHeight))
        var outerPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: util.screenSize().width, height: util.screenSize().height))
        outerPath.appendPath(rectBezierPath)
        outerPath.usesEvenOddFillRule = true
        
        alphaLayer = CAShapeLayer()
        alphaLayer.path = outerPath.CGPath
        alphaLayer.fillRule = kCAFillRuleEvenOdd
        alphaLayer.fillColor = UIColor.blackColor().CGColor
        alphaLayer.opacity = 0.5
        
        instructionTextField.text = (detectionType == util.ScanType.DETECT_BACTERIA) ? "CENTER CONTACT LENSE CASE" : "CENTER SAMPLE WELL"
        let devices = AVCaptureDevice.devices()
        captureSession.sessionPreset = AVCaptureSessionPresetPhoto

        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if(device.position == AVCaptureDevicePosition.Back) {
                    captureDevice = device as? AVCaptureDevice
                }
            }
        }

//        if captureDevice?.hasTorch == true {
//            captureDevice?.lockForConfiguration(nil)
//            captureDevice?.setTorchModeOnWithLevel(1.0, error: nil)
//            captureDevice?.unlockForConfiguration()
//        }
        
        if captureDevice != nil {
            beginSession()
        }
    }//end of viewDidLoad
    
    func setDetectionType(detectionType: util.ScanType){
        self.detectionType = detectionType
    }
    
    func setFrameOnOrientationChange(){
        rectLayer.frame = CGRect(x: util.screenSize().width/2 - dim.rectWidth/2, y: util.screenSize().height/2 - dim.rectHeight/2, width: dim.rectWidth, height: dim.rectHeight)
        previewLayer?.frame = CGRect(x: 0, y: 0, width: util.screenSize().width, height: util.screenSize().height)
        
        var rectBezierPath = UIBezierPath(rect: CGRect(x: util.screenSize().width/2 - dim.rectWidth/2, y: util.screenSize().height/2 - dim.rectHeight/2, width: dim.rectWidth, height: dim.rectHeight))
        var outerPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: util.screenSize().width, height: util.screenSize().height))
        outerPath.appendPath(rectBezierPath)
        outerPath.usesEvenOddFillRule = true
        alphaLayer.path = outerPath.CGPath
        
        let offset : CGFloat = 10
        var tempFrame = cameraCancelButton.frame
        tempFrame.origin.x = util.screenSize().width - tempFrame.width - offset
        tempFrame.origin.y = util.screenSize().height - tempFrame.height - offset
        cameraCancelButton.frame = tempFrame
    }
    
    func cameraCancelled(sender: UIButton!){
        captureSession.stopRunning()
        previewLayer?.removeFromSuperlayer()
        SegInterface.resetDetection()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func beginSession() {
        var err : NSError? = nil
        captureSession.addInput(AVCaptureDeviceInput(device: captureDevice, error: &err))
        
        if err != nil {
            println("error: \(err?.localizedDescription)")
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = self.view.layer.frame
        
        var transition = CATransition()
        transition.delegate = nil;
        transition.duration = 0.3;
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)

        transition.type = kCATransitionFade;
        previewLayer?.addAnimation(transition, forKey: nil)
        
        rectLayer = CALayer()
        rectLayer.frame = CGRect(x: util.screenSize().width/2 - dim.rectWidth/2, y: util.screenSize().height/2 - dim.rectHeight/2, width: dim.rectWidth, height: dim.rectHeight)
        rectLayer.borderColor = UIColor.greenColor().CGColor
        rectLayer.borderWidth = 1.0
        
        println(util.deviceSize().width)
        println(util.deviceSize().height)
        
        self.view.layer.addSublayer(previewLayer)
        self.view.layer.addSublayer(alphaLayer)
        self.view.layer.addSublayer(rectLayer)
        self.view.layer.addSublayer(cameraCancelButton.layer)
        self.view.bringSubviewToFront(instructionTextField)
        
        captureSession.addOutput(captureStillImageOutput)
        captureSession.startRunning()
        
        //initializes timer to take photo
        cameraTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("takePhoto"), userInfo: nil, repeats: true)
    }//end of beginSession

    func takePhoto(){
        var videoConnection : AVCaptureConnection? = nil
        for connection in captureStillImageOutput.connections{
            for port in connection.inputPorts!{
                if port.mediaType == AVMediaTypeVideo {
                    videoConnection = connection as? AVCaptureConnection
                    break;
                }
            }
            if videoConnection != nil {
                break;
            }
        }
        
        if !(captureDevice?.adjustingFocus)! && !(captureDevice?.adjustingWhiteBalance)! && !(captureDevice?.adjustingExposure)! {
            captureStillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: { (buffer: CMSampleBuffer!, err: NSError!) -> Void in
                if(buffer != nil){
                    var imageNSData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                    self.onImageCapture(imageNSData)
                }
            })
        }
    }
    
    func onImageCapture(imageData: NSData!){
        //process and save image
        if(cameraTimer?.valid == false){
            return
        }
        var image = UIImage(data: imageData)
        var imagePointer: UnsafeMutablePointer<UIImage?> = nil

        
        //http://stackoverflow.com/questions/24056205/how-to-use-background-thread-in-swift
        let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
        dispatch_async(backgroundQueue, {
            let isDetection = (self.detectionType == util.ScanType.DETECT_BACTERIA)
            if(!self.done && imagePointer == nil){
                imagePointer = SegInterface.detectLenses(image, num_circles: (isDetection) ? 2 : 1)
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if(imagePointer == nil || self.done){
                    if(imagePointer != nil){
                        SegInterface.dalloc(imagePointer);
                    }
                    return
                }
                self.done = true;
                self.cameraTimer?.invalidate()
                
                let leftLenseImage = imagePointer.memory as UIImage!
                let leftLenseString = UIImageJPEGRepresentation(leftLenseImage as UIImage!, 1).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
                
                var rightLenseString: NSString? = nil
                if(isDetection){
                    let rightLenseImage = imagePointer.successor().memory as UIImage!
                    rightLenseString = UIImageJPEGRepresentation(rightLenseImage as UIImage!, 1).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
                }

                //show result screen
                SegInterface.dalloc(imagePointer)
                self.navigationController?.popToRootViewControllerAnimated(true)
                self.delegate?.scanFinished(image!, leftLenseString: leftLenseString, rightLenseString: rightLenseString)                
            })
        })
    }//end of onImageCapture
    
    func resizeImage(image: UIImage, newSize: CGSize) -> (UIImage) {
        let newRect = CGRectIntegral(CGRectMake(0,0, newSize.width, newSize.height))
        let imageRef = image.CGImage
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // Set the quality level to use when rescaling
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh)
        let flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
        
        CGContextConcatCTM(context, flipVertical)
        // Draw into the context; this scales the image
        CGContextDrawImage(context, newRect, imageRef)
        
        let newImageRef = CGBitmapContextCreateImage(context) as CGImage
        let newImage = UIImage(CGImage: newImageRef)
        
        // Get the resized image from the context and a UIImage
        UIGraphicsEndImageContext()
        
        return newImage!
    }//end of resizeImage
    
//    override func shouldAutorotate() -> Bool {
//        setFrameOnOrientationChange()
//        return true;
//    }
    
    override func viewDidLayoutSubviews() {
        previewLayer?.connection.videoOrientation = videoOrientationFromCurrentDeviceOrientation()
        setFrameOnOrientationChange()
    }
    
    func videoOrientationFromCurrentDeviceOrientation() -> AVCaptureVideoOrientation {
        switch (UIDevice.currentDevice().orientation) {
        case UIDeviceOrientation.Portrait:
            return AVCaptureVideoOrientation.Portrait;
        case UIDeviceOrientation.LandscapeLeft:
            return AVCaptureVideoOrientation.LandscapeRight;
        case UIDeviceOrientation.LandscapeRight:
            return AVCaptureVideoOrientation.LandscapeLeft;
        case UIDeviceOrientation.PortraitUpsideDown:
            return AVCaptureVideoOrientation.PortraitUpsideDown;
        default:
            return AVCaptureVideoOrientation.Portrait;
        }
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if(parent == nil){
            println("camera cancelled")
            captureSession.stopRunning()
            SegInterface.resetDetection()
        }

    }
}