package com.example.raianhuq.bioscantrainer;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.tagmanager.Container;

import java.io.File;
import java.io.IOException;
import java.lang.String;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TrainerScreen extends ActionBarActivity {
    //member variables
    private static final int ACTION_TAKE_PHOTO_B = 1;
    private static final int ACTION_TAKE_PHOTO_S = 2;
    private static final int ACTION_TAKE_VIDEO = 3;

    private String currentPhotoPath;
    private boolean pictureTaken = false;
    private File photoFile;
    private String currentBacteria;
    private String[] bacteriaArray;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "bioscan_" + timeStamp;
        File storageDir = this.getExternalCacheDir();
        File image = null;
        try{
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            image.deleteOnExit();
        }catch(IOException ex){
            Log.e("Pic IOException", "createTempFile FAILED");
            System.out.println("createTempFile FAILED");
        }
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }//end of createImageFile()*/

    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                photoFile = null;
                currentPhotoPath = null;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, actionCode);
            }
        }
    }//end of dispatchTakePictureIntent(int actionCode)*/

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            photoFile.delete();
            finish();
        } else {
//            Uri photoUri = Uri.parse(currentPhotoPath);
//            ImageUploadTask uploadTask = new ImageUploadTask(photoUri, this);
//            uploadTask.execute();
        }
    }//end of onActivityResult(int requestCode, int resultCode, Intent data)*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer);
        //delete files in cache
        File cacheDir = this.getExternalCacheDir();
        File[] files = cacheDir.listFiles();
        if (files != null) {
            for (File file : files)
                if(file.getName().contains("bioscan")) file.delete();
        }

        Button camera = (Button) findViewById(R.id.camera);
        camera.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
                pictureTaken = true;
            }
        });

        Button upload = (Button) findViewById(R.id.upload);
        final TrainerScreen self = this;
        upload.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //run task to upload picture to server
                if(pictureTaken) {
                    Uri photoUri = Uri.parse(currentPhotoPath);
                    ImageUploadTask uploadTask = new ImageUploadTask(photoUri, self);
                    uploadTask.execute();
                    pictureTaken = false;
                }
            }
        });

        bacteriaArray = getResources().getStringArray(R.array.bacteria_names);
        currentBacteria = bacteriaArray[0];

        Spinner spinner = (Spinner) findViewById(R.id.bacteria_choices);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.bacteria_names, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            protected Adapter initializedAdapter=null;
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {
                if(initializedAdapter != parentView.getAdapter() ){
                    initializedAdapter = parentView.getAdapter();
                    return;
                }
                currentBacteria = parentView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // nothing here yet
            }
        });
    }//end of onCreate


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
