package com.example.raianhuq.bioscantrainer;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

public class ImageUploadTask extends AsyncTask<Void, Void, Void> {

    private Uri imageUri;
    private Context context;

    public ImageUploadTask(Uri imageUri, Context context) {
        this.imageUri = imageUri;
        this.context = context;
    }

    ImageUploadTask(){

    }//end of constructor

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }//end of doInBackground()*/

    @Override
    protected void onPostExecute(Void v){
        //Toast
        Toast toast = Toast.makeText(context, "Image Uploaded", 1);
        toast.show();

    }//end of onPostExecute()*/
}
