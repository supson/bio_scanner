//============================================================================
// Name        : testPrediction.cpp
// Author      : Alex Duanmu
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>
#include <vector>
#include <deque>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
using namespace std;

map<string,double> bacteriaToIndex;
map<double,string> indexToBacteria;

const string samplePath = "/home/zduanmu/Pictures/contactLenses/";

void bacteriaIndexInit()
{
	bacteriaToIndex["Achromobacter_xylosoxidans"] = 0;
	bacteriaToIndex["Pseudomonas_aeruginosa"] = 	1;
	bacteriaToIndex["Saline"] = 					2;
	bacteriaToIndex["Staphylococcus_aureus"] = 		3;

	indexToBacteria[0] = "Achromobacter_xylosoxidans";
	indexToBacteria[1] = "Pseudomonas_aeruginosa";
	indexToBacteria[2] = "Saline";
	indexToBacteria[3] = "Staphylococcus_aureus";
}

bool has_suffix(const string& s, const string& suffix)
{
    return (s.size() >= suffix.size()) && equal(suffix.rbegin(), suffix.rend(), s.rbegin());
}

/*function... might want it in some class?*/
int getdir (string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        if ((has_suffix(dirp->d_name, ".png")) || (has_suffix(dirp->d_name, ".jpg")) || (has_suffix(dirp->d_name, ".jpeg")))
        {
            files.push_back(string(dirp->d_name));
        }
    }
    closedir(dp);
    return 0;
}

int main(int argc, char *argv[])
{
	deque<string> cellphones = deque<string>();
	string cellphone = "";
	bacteriaIndexInit();

	if (argc == 2)
	{
		std::string inputModel = argv[1];
		if (inputModel.compare("iphone") == 0)
		{
			cellphones.push_front("iphone");
			cellphone = "iphone";
		}
		else if (inputModel.compare("nexus5") == 0)
		{
			cellphones.push_front("nexus5");
			cellphone = "nexus5";
		}
		else if (inputModel.compare("samsungS4mini") == 0)
		{
			cellphones.push_front("samsungS4mini");
			cellphone = "samsungS4mini";
		}
		else
		{
			cellphones.push_back("iphone");
			cellphones.push_back("nexus5");
			cellphones.push_back("samsungS4mini");
			cout << "The cellphone model cannot be identified. Training default model..." << endl;
		}
	}
	else
	{
		cellphones.push_back("iphone");
		cellphones.push_back("nexus5");
		cellphones.push_back("samsungS4mini");
	}

	for (unsigned int i = 0; i < cellphones.size(); i++)
	{
		string sampleDir = samplePath + cellphones.at(i);

		vector<string> files = vector<string>();

		getdir(sampleDir,files);

		for (unsigned int j = 0;j < files.size();j++) {
			string fileName = sampleDir + "/" + files[j];
			string bacteriaType = files[j].substr(0, files[j].find('#'));
			string cmdString;
			if (cellphone == "")
			{
				cmdString = "./bacteriaPrediction " + fileName + " -l " + bacteriaType;
			}
			else
			{
				cmdString = "./bacteriaPrediction " + fileName + " -l " + bacteriaType + " -m " + cellphone;
			}

			cout << cmdString << endl;
			const char* cmd = cmdString.c_str();
			int status = system(cmd);
		}

	}
}
