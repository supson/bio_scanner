//============================================================================
// Name        : bacteriaPrediction.cpp
// Author      : Alex Duanmu
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <cmath>
#include <map>
#include "svm.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

#define numClusters 4
//#define numFeature 10
#define numFeature 7
#define Malloc(type,n) (type *)malloc((n)*sizeof(type));

svm_node* testnode;
struct svm_model *model;
map<string,double> bacteriaToIndex;
map<double,string> indexToBacteria;

// backteria index initialization
void bacteriaIndexInit()
{
	bacteriaToIndex["Achromobacter_xylosoxidans"] = 0;
	bacteriaToIndex["Pseudomonas_aeruginosa"] = 	1;
	bacteriaToIndex["Saline"] = 					2;
	bacteriaToIndex["Staphylococcus_aureus"] = 		3;

	indexToBacteria[0] = "Achromobacter_xylosoxidans";
	indexToBacteria[1] = "Pseudomonas_aeruginosa";
	indexToBacteria[2] = "Saline";
	indexToBacteria[3] = "Staphylococcus_aureus";
}

void Lab2RGB(double L, double a, double b, double *R, double *G, double *B)
{
	double fY = ( L + 16 ) / 116;
	double fX = a / 500 + fY;
	double fZ = fY - b / 200;

	if ( pow(fY,3) > 0.008856 )
	{
		fY = pow(fY,3);
	}
	else
	{
		fY = ( fY - 16 / 116 ) / 7.787;
	}

	if ( pow(fX,3) > 0.008856 )
	{
		fX = pow(fX,3);
	}
	else
	{
		fX = ( fX - 16 / 116 ) / 7.787;
	}

	if ( pow(fZ,3) > 0.008856 )
	{
		fZ = pow(fZ,3);
	}
	else
	{
		fZ = ( fZ - 16 / 116 ) / 7.787;
	}

	double X = 95.047 * fX;     //ref_X =  95.047     Observer= 2°, Illuminant= D65
	double Y = 100.000 * fY;     //ref_Y = 100.000
	double Z = 108.883 * fZ;     //ref_Z = 108.883

	double X_temp = X / 100;        //X from 0 to  95.047      (Observer = 2°, Illuminant = D65)
	double Y_temp = Y / 100;        //Y from 0 to 100.000
	double Z_temp = Z / 100;        //Z from 0 to 108.883

	double fR = X_temp *  3.2406 + Y_temp * -1.5372 + Z_temp * -0.4986;
	double fG = X_temp * -0.9689 + Y_temp *  1.8758 + Z_temp *  0.0415;
	double fB = X_temp *  0.0557 + Y_temp * -0.2040 + Z_temp *  1.0570;

	if ( fR > 0.0031308 )
	{
		fR = 1.055 * ( pow(fR, 1/2.4) ) - 0.055;
	}
	else
	{
		fR = 12.92 * fR;
	}

	if ( fG > 0.0031308 )
	{
		fG = 1.055 * ( pow(fG, 1/2.4) ) - 0.055;
	}
	else
	{
		fG = 12.92 * fG;
	}

	if ( fB > 0.0031308 )
	{
		fB = 1.055 * ( pow(fB, 1/2.4) ) - 0.055;
	}
	else
	{
		fB = 12.92 * fB;
	}

	*R = fR * 255;
	*G = fG * 255;
	*B = fB * 255;
}

void featureExtraction(Mat image, svm_node* testnode){
	int width = image.cols;
	int height = image.rows;

	Mat floatI(image.size(), CV_32F);
	image.convertTo(floatI, CV_32F);

	Mat grayscaleI(image.size(),CV_32F);
	cvtColor(floatI, grayscaleI, CV_BGR2GRAY);

	// feature 1: luminance
	Scalar scAvg = mean(grayscaleI);
	double dAgv = scAvg.val[0];

	// feature 2-4: Lab of the sample
	Mat Lab(image.size(),CV_8U);
	//floatI *= 1.0/255.0;
	cvtColor(image, Lab, CV_BGR2Lab);

	// vectorize the image to 1 row for each color channel
	Mat samples(width * height, 3, CV_32F);
	  for( int y = 0; y < height; y++ )
	    for( int x = 0; x < width; x++ )
	      for( int z = 0; z < 3; z++)
	        samples.at<float>(y + x*height, z) = Lab.at<Vec3b>(y,x)[z];


	Mat centers;
	Mat labels;
	TermCriteria termcrit(CV_TERMCRIT_ITER, 10, 0.0001);
	kmeans(samples, 3, labels, termcrit, 1, KMEANS_PP_CENTERS, centers);

	//cout << centers << endl;

	int index = 0;
	float max = 0;
	for( int i = 0; i < centers.rows; i++ )
	{
		if (centers.at<float>(i,1) > max)
		{
			max = centers.at<float>(i,1);
			index = i;
		}
	}


	double sampleL = (double)centers.at<float>(index,0) * 100 / 255;
	double sampleA = (double)centers.at<float>(index,1) - 128;
	double sampleB = (double)centers.at<float>(index,2) - 128;

	double R, G, B = 0;
	Lab2RGB(sampleL, sampleA, sampleB, &R, &G, &B);

	testnode[0].index = 1;
	testnode[0].value = dAgv;
	testnode[1].index = 2;
	testnode[1].value = sampleL;
	testnode[2].index = 3;
	testnode[2].value = sampleA;
	testnode[3].index = 4;
	testnode[3].value = sampleB;
	testnode[4].index = 5;
	testnode[4].value = R;
	testnode[5].index = 6;
	testnode[5].value = G;
	testnode[6].index = 7;
	testnode[6].value = B;
	testnode[7].index = -1;
}

int main(int argc, char *argv[])
{

	if (argc < 2) {
		// Tell the user how to run the program
		std::cerr << "Usage: " << argv[0] << " jpegImage" << std::endl;
		std::cerr << "Optional input:" << std::endl;
		std::cerr << "-m: <cellphone_model> (available model: iphone, nexus5, samsungS4mini)" << std::endl;
		std::cerr << "-l: <bacteria_label> (available label: Saline, Staphylococcus_aureus, Achromobacter_xylosoxidans, Pseudomonas_aeruginosa)" << std::endl;
		/* "Usage messages" are a conventional way of telling the user
		* how to run a program if they enter the command incorrectly.
		*/
		return -1;
	}

	string cellphoneModel = "defaultSVMModel.model";
	std::string bacteriaLabel = "";
	bacteriaIndexInit();


	for (int i = 2; i < argc; i++)
	{
		std::string arg = argv[i];
		if (arg == "-l")
		{
			if (i + 1 < argc)
			{
				std::string inputLabel = argv[++i];
				if (inputLabel.compare("Saline") == 0)
				{
					bacteriaLabel = "Saline";
				}
				else if (inputLabel.compare("Staphylococcus_aureus") == 0)
				{
					bacteriaLabel = "Staphylococcus_aureus";
				}
				else if (inputLabel.compare("Achromobacter_xylosoxidans") == 0)
				{
					bacteriaLabel = "Achromobacter_xylosoxidans";
				}
				else if (inputLabel.compare("Pseudomonas_aeruginosa") == 0)
				{
					bacteriaLabel = "Pseudomonas_aeruginosa";
				}
				else
				{
					std::cerr << "The bacteria label cannot be identified." << endl;
					return -1;
				}
			}
			else
			{
				std::cerr << "-l option requires one argument." << endl;
				return -1;
			}
		}
		else if (arg == "-m")
		{
			if (i + 1 < argc)
			{
				std::string inputModel = argv[++i];
				if (inputModel.compare("iphone") == 0)
				{
					cellphoneModel = "iphoneSVMModel.model";
				}
				else if (inputModel.compare("nexus5") == 0)
				{
					cellphoneModel = "nexus5SVMModel.model";
				}
				else if (inputModel.compare("samsungS4mini") == 0)
				{
					cellphoneModel = "samsungS4miniSVMModel.model";
				}
				else
				{
					cout << "The cellphone model cannot be identified. Using default model..." << endl;
					cellphoneModel = "defaultSVMModel.model";
				}
			}
			else
			{
				std::cerr << "-m option requires one argument." << endl;
				return -1;
			}
		}
		else
		{
			std::cerr << "Invalid input." << endl;
			return -1;
		}
	}


	Mat image;
	image = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file

	if(! image.data )                              // Check for invalid input
	{
		cout <<  "Could not open or find the image" << endl;
	    return -1;
	}

	svm_node* testnode = Malloc(svm_node,numFeature+1);
	featureExtraction(image, testnode);

	// read proper svm model
	const char *model_file_name = cellphoneModel.c_str();
	model = svm_load_model(model_file_name);
	// svm predict
	double *probability = Malloc(double,numClusters);
	double bacteriaIndex = svm_predict_probability(model, testnode, probability);
	string predictedBacteria = indexToBacteria.at(bacteriaIndex);
	cout << predictedBacteria << "," << probability[(int)bacteriaIndex] << endl;

	if (bacteriaLabel != "")
	{
		if (bacteriaToIndex.at(bacteriaLabel) != bacteriaIndex)
		{
			cout << "Accuracy = 0%" << endl;
		}
		else
		{
			cout << "Accuracy = 100%" << endl;
		}
	}

	free(probability);
	free(testnode);
	free(model);


	return 0;
}
