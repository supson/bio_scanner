//============================================================================
// Name        : bacteriaPrediction.cpp
// Author      : Alex Duanmu
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "svm.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

#define numFeature 7
#define Malloc(type,n) (type *)malloc((n)*sizeof(type));

void featureExtraction(Mat image, svm_node* testnode){
	int width = image.cols;
	int height = image.rows;
	int numChannels = image.channels();

	Mat floatI(image.size(), CV_32F);
	image.convertTo(floatI, CV_32F);

	cout << "src1 intensity = " << (int) image.at<Vec3b>(0,0)[1] << endl << "dst intensity = " << floatI.at<Vec3f>(0,0)[1] <<endl << endl;

	Mat grayscaleI(image.size(),CV_32F);
	cvtColor(floatI, grayscaleI, CV_BGR2GRAY);

	// feature 1: luminance
	Scalar scAvg = mean(grayscaleI);
	double dAgv = scAvg.val[0];

	// feature 2-4: Lab of the sample
	Mat Lab(image.size(),CV_8U);
	//image *= 1.0/255.0;
	cvtColor(image, Lab, CV_BGR2Lab);

	cout << "Lab L value = " << (float) Lab.at<Vec3b>(0,0)[0] << endl;
	cout << "Lab a value = " << (float) Lab.at<Vec3b>(0,0)[1] << endl;
	cout << "Lab b value = " << (float) Lab.at<Vec3b>(0,0)[2] << endl;
	cout << "Lab L value = " << (float) Lab.at<Vec3b>(100,100)[0] << endl;
	cout << "Lab a value = " << (float) Lab.at<Vec3b>(100,100)[1] << endl;
	cout << "Lab b value = " << (float) Lab.at<Vec3b>(100,100)[2] << endl;

	Mat floatLab(image.size(), CV_32F);
	Lab.convertTo(floatLab, CV_32F);

	std::vector<cv::Mat> labChannels(3);
	split(floatLab, labChannels);

	cout << "labChannels L value = " << (float) labChannels[0].at<float>(0,0) << endl;
	cout << "labChannels a value = " << (float) labChannels[1].at<float>(0,0) << endl;
	cout << "labChannels b value = " << (float) labChannels[2].at<float>(0,0) << endl;
	cout << "labChannels L value = " << (float) labChannels[0].at<float>(100,100) << endl;
	cout << "labChannels a value = " << (float) labChannels[1].at<float>(100,100) << endl;
	cout << "labChannels b value = " << (float) labChannels[2].at<float>(100,100) << endl;

	labChannels[0] = labChannels[0] * (100.0/255.0);
	labChannels[1] = labChannels[1] - 128;
	labChannels[2] = labChannels[2] - 128;

	cout << "after normalization..." << endl;
	cout << "labChannels L value = " << (float) labChannels[0].at<float>(0,0) << endl;
	cout << "labChannels a value = " << (float) labChannels[1].at<float>(0,0) << endl;
	cout << "labChannels b value = " << (float) labChannels[2].at<float>(0,0) << endl;
	cout << "labChannels L value = " << (float) labChannels[0].at<float>(100,100) << endl;
	cout << "labChannels a value = " << (float) labChannels[1].at<float>(100,100) << endl;
	cout << "labChannels b value = " << (float) labChannels[2].at<float>(100,100) << endl;

	merge(labChannels,floatLab);

	cout << "floatLab L value = " << (float) floatLab.at<Vec3b>(0,0)[0] << endl;
	cout << "floatLab a value = " << (float) floatLab.at<Vec3b>(0,0)[1] << endl;
	cout << "floatLab b value = " << (float) floatLab.at<Vec3b>(0,0)[2] << endl;
	cout << "floatLab L value = " << (float) floatLab.at<Vec3b>(100,100)[0] << endl;
	cout << "floatLab a value = " << (float) floatLab.at<Vec3b>(100,100)[1] << endl;
	cout << "floatLab b value = " << (float) floatLab.at<Vec3b>(100,100)[2] << endl;

	// vectorize the image to 1 row for each color channel
	Mat colVec = floatLab.reshape(1, width*height); // change to a Nx3 column vector
	cout << "colVec is of size: " << colVec.rows << "x" << colVec.cols << endl;

	Mat centers;
	Mat labels;
	TermCriteria termcrit(CV_TERMCRIT_ITER, 10, 1.0);
	kmeans(colVec, 3, labels, termcrit, 10, KMEANS_PP_CENTERS, centers);

	cout << "centers value L1 = " << (float) centers.at<Vec3b>(0,0)[0] << endl;
	cout << "centers value a1 = " << (float) centers.at<Vec3b>(0,0)[1] << endl;
	cout << "centers value b1 = " << (float) centers.at<Vec3b>(0,0)[2] << endl;

	cout << "centers value L2 = " << (float) centers.at<Vec3b>(1,0)[0] << endl;
	cout << "centers value a2 = " << (float) centers.at<Vec3b>(1,0)[1] << endl;
	cout << "centers value b2 = " << (float) centers.at<Vec3b>(1,0)[2] << endl;

	cout << "centers value L3 = " << (float) centers.at<Vec3b>(2,0)[0] << endl;
	cout << "centers value a3 = " << (float) centers.at<Vec3b>(2,0)[1] << endl;
	cout << "centers value b3 = " << (float) centers.at<Vec3b>(2,0)[2] << endl;

	Mat avgL, avgA, avgB;
	vector<Mat> channels(3);
	split(centers, channels);
	avgL = channels[0];
	avgA = channels[1];
	avgB = channels[2];

	double minVal;
	double maxVal;
	Point minLoc;
	Point maxLoc;

	minMaxLoc( avgA, &minVal, &maxVal, &minLoc, &maxLoc );

	double sampleL = (double)centers.at<Vec3b>(maxLoc)[0];
	double sampleA = (double)centers.at<Vec3b>(maxLoc)[1];
	double sampleB = (double)centers.at<Vec3b>(maxLoc)[2];

	//feature 5-7: R G B
	Mat LabSample(1,1,CV_8U);
	LabSample.at<unsigned char>(0,0)[0] = (unsigned char)(sampleL * 255.0 / 100.0);
	LabSample.at<unsigned char>(0,0)[1] = (unsigned char)(sampleA + 128);
	LabSample.at<unsigned char>(0,0)[2] = (unsigned char)(sampleB + 128);
	Mat RGBSample;
	//image *= 1.0/255.0;
	cvtColor(LabSample, RGBSample, CV_Lab2RGB);

	testnode[0].index = 0;
	testnode[0].value = dAgv;
	testnode[1].index = 1;
	testnode[1].value = (double)RGBSample.at<unsigned char>(0,0)[0];
	testnode[2].index = 2;
	testnode[2].value = (double)RGBSample.at<unsigned char>(0,0)[1];
	testnode[3].index = 3;
	testnode[3].value = (double)RGBSample.at<unsigned char>(0,0)[2];
	testnode[4].index = 4;
	testnode[4].value = sampleL;
	testnode[5].index = 5;
	testnode[5].value = sampleA;
	testnode[6].index = 6;
	testnode[6].value = sampleB;
	testnode[7].index = -1;
}

int main(int argc, char *argv[])
{

	if (argc < 2) {
		// Tell the user how to run the program
		std::cerr << "Usage: " << argv[0] << " jpegImage" << std::endl;
		std::cerr << "Optional input:" << std::endl;
		std::cerr << "-m: <cellphone_model> (available model: iphone, nexus5, samsungS4mini)" << std::endl;
		std::cerr << "-l: <bacteria_label> (available label: Saline, Staphylococcus_aureus, Achromobacter_xylosoxidans, Pseudomonas_aeruginosa)" << std::endl;
		std::cerr << "Optional input:" << std::endl;
		/* "Usage messages" are a conventional way of telling the user
		* how to run a program if they enter the command incorrectly.
		*/
		return -1;
	}

	std::string cellphoneModel = "";
	std::string bacteriaLabel = "";

	for (int i = 2; i < argc; i++)
	{
		std::string arg = argv[i];
		if (arg == "-l")
		{
			if (i + 1 < argc)
			{
				std::string inputLabel = argv[++i];
				if (inputLabel.compare("Saline") != 0)
				{
					bacteriaLabel = "Saline";
				}
				else if (inputLabel.compare("Staphylococcus_aureus") != 0)
				{
					bacteriaLabel = "Staphylococcus_aureus";
				}
				else if (inputLabel.compare("Achromobacter_xylosoxidans") != 0)
				{
					bacteriaLabel = "Achromobacter_xylosoxidans";
				}
				else if (inputLabel.compare("Pseudomonas_aeruginosa") != 0)
				{
					bacteriaLabel = "Pseudomonas_aeruginosa";
				}
				else
				{
					std::cerr << "The bacteria label cannot be identified." << endl;
					return -1;
				}
			}
			else
			{
				std::cerr << "-l option requires one argument." << endl;
				return -1;
			}
		}
		else if (arg == "-m")
		{
			if (i + 1 < argc)
			{
				std::string inputModel = argv[++i];
				if (inputModel.compare("iphone") != 0)
				{
					cellphoneModel = "iphone";
				}
				else if (inputModel.compare("nexus5") != 0)
				{
					cellphoneModel = "nexus5";
				}
				else if (inputModel.compare("samsungS4mini") != 0)
				{
					cellphoneModel = "samsungS4mini";
				}
				else
				{
					std::cerr << "The cellphone model cannot be identified." << endl;
					return -1;
				}
			}
			else
			{
				std::cerr << "-m option requires one argument." << endl;
				return -1;
			}
		}
		else
		{
			std::cerr << "Invalid input." << endl;
			return -1;
		}
	}

	Mat image;
	image = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file

	if(! image.data )                              // Check for invalid input
	{
	        cout <<  "Could not open or find the image" << std::endl;
	        return -1;
	}

	svm_node* testnode = Malloc(svm_node,numFeature+1);
	featureExtraction(image, testnode);

	// read proper svm model

	// svm predict


	return 0;
}

