//============================================================================
// Name        : trainModel.cpp
// Author      : Alex Duanmu
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <deque>
#include <vector>
#include <dirent.h>
#include <errno.h>
#include "svm.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

//#define numFeature 10
#define numFeature 7
#define Malloc(type,n) (type *)malloc((n)*sizeof(type));

struct svm_parameter param;
struct svm_problem prob;
struct svm_model *model;
struct svm_node *x_space;
struct svm_node **node;

map<string,double> bacteriaToIndex;
map<double,string> indexToBacteria;

const string samplePath = "/home/zduanmu/Pictures/contactLenses/";

// backteria index initialization
void bacteriaIndexInit()
{
	bacteriaToIndex["Achromobacter_xylosoxidans"] = 0;
	bacteriaToIndex["Pseudomonas_aeruginosa"] = 	1;
	bacteriaToIndex["Saline"] = 					2;
	bacteriaToIndex["Staphylococcus_aureus"] = 		3;

	indexToBacteria[0] = "Achromobacter_xylosoxidans";
	indexToBacteria[1] = "Pseudomonas_aeruginosa";
	indexToBacteria[2] = "Saline";
	indexToBacteria[3] = "Staphylococcus_aureus";
}

void Lab2RGB(double L, double a, double b, double *R, double *G, double *B)
{
	double fY = ( L + 16 ) / 116;
	double fX = a / 500 + fY;
	double fZ = fY - b / 200;

	if ( pow(fY,3) > 0.008856 )
	{
		fY = pow(fY,3);
	}
	else
	{
		fY = ( fY - 16 / 116 ) / 7.787;
	}

	if ( pow(fX,3) > 0.008856 )
	{
		fX = pow(fX,3);
	}
	else
	{
		fX = ( fX - 16 / 116 ) / 7.787;
	}

	if ( pow(fZ,3) > 0.008856 )
	{
		fZ = pow(fZ,3);
	}
	else
	{
		fZ = ( fZ - 16 / 116 ) / 7.787;
	}

	double X = 95.047 * fX;     //ref_X =  95.047     Observer= 2°, Illuminant= D65
	double Y = 100.000 * fY;     //ref_Y = 100.000
	double Z = 108.883 * fZ;     //ref_Z = 108.883

	double X_temp = X / 100;        //X from 0 to  95.047      (Observer = 2°, Illuminant = D65)
	double Y_temp = Y / 100;        //Y from 0 to 100.000
	double Z_temp = Z / 100;        //Z from 0 to 108.883

	double fR = X_temp *  3.2406 + Y_temp * -1.5372 + Z_temp * -0.4986;
	double fG = X_temp * -0.9689 + Y_temp *  1.8758 + Z_temp *  0.0415;
	double fB = X_temp *  0.0557 + Y_temp * -0.2040 + Z_temp *  1.0570;

	if ( fR > 0.0031308 )
	{
		fR = 1.055 * ( pow(fR, 1/2.4) ) - 0.055;
	}
	else
	{
		fR = 12.92 * fR;
	}

	if ( fG > 0.0031308 )
	{
		fG = 1.055 * ( pow(fG, 1/2.4) ) - 0.055;
	}
	else
	{
		fG = 12.92 * fG;
	}

	if ( fB > 0.0031308 )
	{
		fB = 1.055 * ( pow(fB, 1/2.4) ) - 0.055;
	}
	else
	{
		fB = 12.92 * fB;
	}

	*R = fR * 255;
	*G = fG * 255;
	*B = fB * 255;
}

void featureExtraction(Mat image, svm_node* testnode){
	int width = image.cols;
	int height = image.rows;

	Mat floatI(image.size(), CV_32F);
	image.convertTo(floatI, CV_32F);

	Mat grayscaleI(image.size(),CV_32F);
	cvtColor(floatI, grayscaleI, CV_BGR2GRAY);

	// feature 1: luminance
	Scalar scAvg = mean(grayscaleI);
	double dAgv = scAvg.val[0];

	// feature 2-4: Lab of the sample
	Mat Lab(image.size(),CV_8U);
	//floatI *= 1.0/255.0;
	cvtColor(image, Lab, CV_BGR2Lab);

	// vectorize the image to 1 row for each color channel
	Mat samples(width * height, 3, CV_32F);
	  for( int y = 0; y < height; y++ )
	    for( int x = 0; x < width; x++ )
	      for( int z = 0; z < 3; z++)
	        samples.at<float>(y + x*height, z) = Lab.at<Vec3b>(y,x)[z];


	Mat centers;
	Mat labels;
	TermCriteria termcrit(CV_TERMCRIT_ITER, 10, 0.0001);
	kmeans(samples, 3, labels, termcrit, 1, KMEANS_PP_CENTERS, centers);

	//cout << centers << endl;

	int index = 0;
	float max = 0;
	for( int i = 0; i < centers.rows; i++ )
	{
		if (centers.at<float>(i,1) > max)
		{
			max = centers.at<float>(i,1);
			index = i;
		}
	}


	double sampleL = (double)centers.at<float>(index,0) * 100 / 255;
	double sampleA = (double)centers.at<float>(index,1) - 128;
	double sampleB = (double)centers.at<float>(index,2) - 128;

	double R, G, B = 0;
	Lab2RGB(sampleL, sampleA, sampleB, &R, &G, &B);

	testnode[0].index = 1;
	testnode[0].value = dAgv;
	testnode[1].index = 2;
	testnode[1].value = sampleL;
	testnode[2].index = 3;
	testnode[2].value = sampleA;
	testnode[3].index = 4;
	testnode[3].value = sampleB;
	testnode[4].index = 5;
	testnode[4].value = R;
	testnode[5].index = 6;
	testnode[5].value = G;
	testnode[6].index = 7;
	testnode[6].value = B;
	testnode[7].index = -1;
}

bool has_suffix(const string& s, const string& suffix)
{
    return (s.size() >= suffix.size()) && equal(suffix.rbegin(), suffix.rend(), s.rbegin());
}

/*function... might want it in some class?*/
int getdir (string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        if ((has_suffix(dirp->d_name, ".png")) || (has_suffix(dirp->d_name, ".jpg")) || (has_suffix(dirp->d_name, ".jpeg")))
        {
            files.push_back(string(dirp->d_name));
        }
    }
    closedir(dp);
    return 0;
}

void svmInit(int numCluster)
{
	param.svm_type = C_SVC;
	param.kernel_type = LINEAR;
	param.degree = 3;
	param.gamma = 1/numFeature;
	param.coef0 = 0;
	param.nu = 0.5;
	param.cache_size = 100;
	param.C = 1;
	param.eps = 1e-3;
	param.p = 0.1;
	param.shrinking = 1;
	param.probability = 1;
	param.nr_weight = 0;
	param.weight_label = NULL;
	param.weight = NULL;
}

void analysisData() {
	const char *error_msg;
	error_msg = svm_check_parameter(&prob,&param);
	if(error_msg)
	{
		printf("\nerror:%s\n", error_msg);
		exit(1);
	}

	model = svm_train(&prob, &param);
}

int main(int argc, char *argv[])
{
	deque<string> cellphones = deque<string>();
	string modelFile = "";
	bacteriaIndexInit();

	if (argc == 2)
	{
		std::string inputModel = argv[1];
		if (inputModel.compare("iphone") == 0)
		{
			cellphones.push_front("iphone");
			modelFile = "iphoneSVMModel.model";
		}
		else if (inputModel.compare("nexus5") == 0)
		{
			cellphones.push_front("nexus5");
			modelFile = "nexus5SVMModel.model";
		}
		else if (inputModel.compare("samsungS4mini") == 0)
		{
			cellphones.push_front("samsungS4mini");
			modelFile = "samsungS4miniSVMModel.model";
		}
		else
		{
			cout << "The cellphone model cannot be identified. Training default model..." << endl;
			cellphones.push_back("iphone");
			cellphones.push_back("nexus5");
			cellphones.push_back("samsungS4mini");
			modelFile = "defaultSVMModel.model";
		}
	}
	else
	{
		cellphones.push_back("iphone");
		cellphones.push_back("nexus5");
		cellphones.push_back("samsungS4mini");
		modelFile = "defaultSVMModel.model";
	}

	svmInit(4);

	// get the total number of samples
	int numSamples = 0;
	for (unsigned int i = 0; i < cellphones.size(); i++)
	{
		string sampleDir = samplePath + cellphones.at(i);

		vector<string> files = vector<string>();

		getdir(sampleDir,files);

		for (unsigned int j = 0;j < files.size();j++) {
			numSamples += 1;
		}

	}
	// allocate memory for each sample node
	node = Malloc(struct svm_node*, numSamples);
	// allocate memory for all sample label
	double *y1 = Malloc(double, numSamples);

	// feature extraction
	// save to node
	int sampleIndex = 0;
	for (unsigned int i = 0; i < cellphones.size(); i++)
	{
		string sampleDir = samplePath + cellphones.at(i);

		vector<string> files = vector<string>();

		getdir(sampleDir,files);

		sort(files.begin(), files.end());

		for (unsigned int j = 0;j < files.size();j++) {
			string fileName = sampleDir + "/" + files[j];
			string bacteriaType = files[j].substr(0, files[j].find('#'));
			x_space = Malloc(svm_node, numFeature + 1);
			Mat image = imread(fileName);
			cout << "input sample type " << bacteriaType << endl;
			cout << "Extraction feature for sample " << sampleIndex << endl;
			featureExtraction(image, x_space);
			node[sampleIndex] = x_space;
			y1[sampleIndex] = bacteriaToIndex.at(bacteriaType);
			sampleIndex++;
		}

	}

	prob.l = numSamples;
	prob.x = node;
	prob.y = y1;

	analysisData();

	const char *model_file_name = modelFile.c_str();
	int status = svm_save_model(model_file_name, model);
	if (status != 0)
	{
		cout << "The SVM saving has failed with an error code: " << status << endl;
	}

	for (int j = 0;j < numSamples;j++) {
		free(node[j]);
	}
	free(node);
	free(y1);
	free(model);

	cout << "The SVM training has completed." << status << endl;

	return 0;
}
