from django.db import models

# Create your models here.
class Document(models.Model):
    docfile = models.FileField(upload_to='images/')

class PredictionResult(models.Model):
	timestamp=models.DateTimeField('timestamp',auto_now_add=True);
	bacteria1=models.CharField(null=True,max_length=50);
	bacteria2=models.CharField(null=True,max_length=50);
	phone=models.CharField(null=True,max_length=50);
	prob1=models.DecimalField(null=True,decimal_places=5,max_digits=8);
	prob2=models.DecimalField(null=True,decimal_places=5,max_digits=8);
	id=models.AutoField(primary_key=True);
	
