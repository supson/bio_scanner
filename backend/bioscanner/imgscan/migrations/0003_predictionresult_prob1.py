# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('imgscan', '0002_predictionresult'),
    ]

    operations = [
        migrations.AddField(
            model_name='predictionresult',
            name='prob1',
            field=models.DecimalField(null=True, max_digits=8, decimal_places=5),
        ),
    ]
