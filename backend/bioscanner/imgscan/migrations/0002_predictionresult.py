# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('imgscan', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PredictionResult',
            fields=[
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name=b'timestamp')),
                ('bacteria1', models.CharField(max_length=50, null=True)),
                ('bacteria2', models.CharField(max_length=50, null=True)),
                ('phone', models.CharField(max_length=50, null=True)),
                ('prob2', models.DecimalField(null=True, max_digits=8, decimal_places=5)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
            ],
        ),
    ]
