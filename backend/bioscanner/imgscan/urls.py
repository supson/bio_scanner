from django.conf.urls import patterns, url

urlpatterns = patterns('imgscan.views',
    url(r'^process/$', 'process', name='process'),
    url(r'^retreive/(?P<result_id>[0-9]+)$', 'retrieve', name='retrieve'),
)
