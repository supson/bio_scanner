from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import serializers
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from imgscan.models import PredictionResult
from decimal import Decimal

import base64
import sys
from subprocess import Popen,PIPE,call

@api_view(['GET','POST'])
def process(request):
	if request.method == 'POST':
		#newdoc = Document(docfile = request.FILES['docfile'])
		#newdoc.save()
		print('got post')
		if 'img1' in request.data:
			image=base64.b64decode(request.data['img1'])
			print('got image size=%d' %sys.getsizeof(image))
			fileHandle=open("./IMGDIR/"+'tempImg1.jpg','wb')
			fileHandle.write(image)
			fileHandle.close()
			cmd = "cd ./imgscan/detectionBins;./bacteriaPrediction ../../IMGDIR/tempImg1.jpg"
			p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
			out, err = p.communicate()
			b1=out.rstrip().split(',')[0];
			p1=Decimal(out.rstrip().split(',')[1].strip('"'))
			print(b1)
			print(p1)
			#save to db
			r=PredictionResult(bacteria1=b1,prob1=p1);
			if 'img2' not in request.data:
				r.save()
				return Response({'bacteria1':out.rstrip().split(',')[0],'probability1':float(out.rstrip().split(',')[1].strip('"')),'bacteria2':'','probability2':'','ok':1,'id':r.id})
			

		#2nd img
		if 'img2' in request.data:
			image=base64.b64decode(request.data['img2'])
			print('got image size=%d' %sys.getsizeof(image))
			fileHandle=open("./IMGDIR/"+'tempImg2.jpg','wb')
			fileHandle.write(image)
			fileHandle.close()
			cmd = "cd ./imgscan/detectionBins;./bacteriaPrediction ../../IMGDIR/tempImg2.jpg"
			p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
			out2, err2 = p.communicate()
			b2=out2.rstrip().split(',')[0];
			p2=Decimal(out2.rstrip().split(',')[1].strip('"'))
			print(b2)
			print(p2)
			#save to db
			r.bacteria2=b2
			r.prob2=p2
			r.save()
			return Response({'bacteria1':out.rstrip().split(',')[0],'probability1':float(out.rstrip().split(',')[1].strip('"')),'bacteria2':out2.rstrip().split(',')[0],'probability2':float(out2.rstrip().split(',')[1].strip('"')),'ok':1,'id':r.id})

	else:
		print("err")
		return Response("err")

@api_view(['GET'])
def retrieve(request,result_id):
	r=PredictionResult.objects.all()[int(float(result_id))];
	print("got get")
	rsp={'bacteria1':r.bacteria1,'probability1':r.prob1,'bacteria2':r.bacteria2,'probability2':r.prob2,'id':r.id}
	print(rsp)
	return Response(rsp)
